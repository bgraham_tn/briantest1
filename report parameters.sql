use impresario
go 

select a.user_id, a.id request_id--, a.report_id
, e.description folder
, d.name report_name, a.request_date_time, c.sequence_number sort
, c.description parameter, b.value
from gooesoft_request a
join gooesoft_request_parameter b on a.id = b.request_id 
join gooesoft_report_parameter c on a.report_id = c.report_id and b.parameter_id = c.id
join gooesoft_report d on a.report_id = d.id
join gooesoft_report_category e on d.category = e.id
where 1=1
and a.result_code is not null --ignore scheduled runs that haven't happened yet 
and cast(a.request_date_time as date) >= cast(getdate() as date) --Today
and a.user_id = 'bgraham'
--and a.report_id = 'contribution_import'
--and d.name = 'Manage Attributes'
order by a.request_date_time desc, c.sequence_number
--hi dad